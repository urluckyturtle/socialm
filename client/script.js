
var app = angular.module("socialm", ["ngRoute"]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "main.html"
    })
    .when("/signin", {
      templateUrl: "signin.html"
    })
    .when("/signup", {
      templateUrl: "signup.html"
    })
    .when("/profile", {
      templateUrl: "profile.html"
    })
    .when("/posts/:postId", {
      templateUrl: "post.html"
    });
});

app.controller("socialCtrl", function ($scope, $location, $http) {
  $scope.signup = function () {
    if ($scope.socialpass == $scope.socialpassc) {
      $http.post('/api/signup', { username: $scope.socialusername, password: $scope.socialpass })
        .then(function (result) {
          console.log(result);
          if (result.data == "successfully signed up") {
            $location.path('/signin');
          }
          else {


            $scope.unmatched = result.data;
          }
        }, function (error) {
          console.log(error);
        });
    }
    else {
      $scope.unmatched = "passwords not same";

    }

  }
});

app.controller("loginCtrl", function ($scope, $location, $http) {

  $scope.signin = function () {
    $http.post('/api/signin', { username: $scope.socialusername, password: $scope.socialpass })
      .then(function (result) {
        console.log(result, 'success')
        if (result.data == "successfully logged in") {
          $location.path('/');
        }
        else {


          $scope.unmatched = result.data;
        }

      });
  }


});