const express = require('express');
const path = require('path');
let jwt = require('jsonwebtoken');
let config = require('../config');
let middleware = require('../middleware');

const bodyParser = require('body-parser');
const app = express();
const port = 3000;
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/socialm', { useNewUrlParser: true });

const clientFolderPath = path.join(__dirname, '..', 'client/');
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'html');
var userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }

});

var userdata = mongoose.model('userdatas', userSchema);
app.use(express.json());// for parsing application/json
app.use(express.urlencoded({ extended: true }));

app.use('/', express.static(clientFolderPath));

// app.use('/login', (req, res) => {
//   res.render('login', { title: 'Sign In' });
// });

app.post('/api/signup', (req, res) => {
  var newuser = new userdata(req.body);
  userdata.findOne({ username: newuser.username }, function (err, adventure) {
    if (err) {
      return console.error(err)
    }
    else {
      if (adventure == null) {
        newuser.save(function (err, book) {
          if (err) return console.error(err);
          console.log(newuser.username + " successfully signed up.");
          res.send('successfully signed up')
        });
      }
      else {
        res.send('Username already taken')
      };
    };

  });

  //res.send('hi');
})


app.post('/api/signin', (req, res) => {
  var user = req.body;
  //console.log(user);
  userdata.findOne({ username: user.username, password: user.password }, function (err, adventure) {
    if (err) {
      return console.error(err)
    }
    else {
      if (adventure == null) {
        console.log("not there");
        res.send("Either not there or password is wrong");
      }
      else {
        res.send("successfully logged in")
      }

    }
  });
});

//app.get('/', (req, res) => res.send('Hello World!'))

app.use('/*', express.static(clientFolderPath));


app.listen(port, () => console.log(`Example app listening on port ${port}!`));